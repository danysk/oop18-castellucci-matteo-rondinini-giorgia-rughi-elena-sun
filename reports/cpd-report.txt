Found a 13 line (118 tokens) duplication in the following files: 
Starting at line 304 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 330 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

    public void worldMovementRightOnLadderTransmission() {
        this.world.initLevel(Arrays.asList(this.platformProperties, this.playerProperties, this.ladderProperties));
        final Optional<UnmodifiableEntity> player = this.getPlayer();
        /* updates to let the player fall and touch the platform, if it was created slightly above the platform*/
        for (int i = 0; i < SHORT_UPDATE_STEPS; i++) {
            this.world.update();
        }
        this.world.movePlayer(MovementType.CLIMB_UP);
        for (int i = 0; i < 2 * LONG_UPDATE_STEPS; i++) {
            this.world.update();
        }
        final Pair<Double, Double> playerTopPosition = player.get().getPosition();
        this.world.movePlayer(MovementType.MOVE_RIGHT);
=====================================================================
Found a 16 line (104 tokens) duplication in the following files: 
Starting at line 105 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/model/entities/AbstractEntity.java
Starting at line 113 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/model/physics/AbstractPhysicalBody.java

                + "; Position: (" + this.getPosition().getLeft() + ", " + this.getPosition().getRight()
                + "); Dimensions: " + this.getDimensions().getLeft() + "x" + this.getDimensions().getRight()
                + "; Angle: " + this.getAngle();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        return obj != null
                && this.getClass().equals(obj.getClass())
                && this.body.equals(((AbstractEntity) obj).body);
=====================================================================
Found a 8 line (93 tokens) duplication in the following files: 
Starting at line 53 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldCollisionsTest.java
Starting at line 68 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

    public WorldCollisionsTest() {
        this.platformProperties 
            = new EntityPropertiesImpl(EntityType.PLATFORM, BodyShape.RECTANGLE, WORLD_WIDTH / 2, WORLD_HEIGHT / 2, 
                                       PLATFORM_WIDTH, PLATFORM_HEIGHT, ANGLE, Optional.absent(), Optional.absent());
        this.playerProperties 
            = new EntityPropertiesImpl(EntityType.PLAYER, BodyShape.RECTANGLE, WORLD_WIDTH / 2,
                                       WORLD_HEIGHT / 2 + PLATFORM_HEIGHT / 2 + PLAYER_DIMENSION / 2, PLAYER_DIMENSION, 
                                       PLAYER_DIMENSION, ANGLE, Optional.absent(), Optional.absent());
=====================================================================
Found a 18 line (91 tokens) duplication in the following files: 
Starting at line 29 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/DynamicPhysicalBodyCreationTest.java
Starting at line 32 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/StaticPhysicalBodyCreationTest.java

public class DynamicPhysicalBodyCreationTest {
    private static final double WORLD_WIDTH = 8;
    private static final double WORLD_HEIGHT = 4.5;
    private static final double STD_WIDTH = WORLD_WIDTH / 15;
    private static final double STD_HEIGHT = WORLD_HEIGHT / 15;
    private static final ImmutablePair<Double, Double> STD_POSITION = new ImmutablePair<>(WORLD_WIDTH / 2, WORLD_HEIGHT / 2);
    private static final double STD_DIMENSION = WORLD_WIDTH / 8;
    private static final String NULL_BODY_MSG = "Instead of a PhysicalBody, null was returned";

    private final Map<EntityType, BodyShape> allowedCombinations;
    private final EntityType consideredType;
    private final PhysicalFactory factory;

    /**
     * Builds a new {@link DynamicPhysicalBodyCreationTest} for the given {@link EntityType}.
     * @param type the {@link EntityType} to test
     */
    public DynamicPhysicalBodyCreationTest(final EntityType type) {
=====================================================================
Found a 8 line (83 tokens) duplication in the following files: 
Starting at line 190 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/EntityCreationTest.java
Starting at line 213 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/EntityCreationTest.java
Starting at line 237 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/EntityCreationTest.java

                                .setPowerUpType(Optional.of(PowerUpType.EXTRA_LIFE));
        final PowerUp powerUp = powerUpBuilder.build();
        assertEquals(GIVEN_EQUALS_SET_MSG, STD_POSITION, powerUp.getPosition());
        assertEquals(GIVEN_EQUALS_SET_MSG, STD_RECTANGULAR_DIMENSIONS, powerUp.getDimensions());
        assertEquals(GIVEN_EQUALS_SET_MSG, STD_ANGLE, powerUp.getAngle(), PRECISION);
        assertEquals(GIVEN_EQUALS_SET_MSG, BodyShape.RECTANGLE, powerUp.getShape());
        assertEquals(GIVEN_EQUALS_SET_MSG, EntityType.POWERUP, powerUp.getType());
        assertEquals(GIVEN_EQUALS_SET_MSG, PowerUpType.EXTRA_LIFE, powerUp.getPowerUpType());
=====================================================================
Found a 15 line (81 tokens) duplication in the following files: 
Starting at line 316 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 342 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

        this.world.movePlayer(MovementType.MOVE_RIGHT);
        for (int i = 0; i < SHORT_UPDATE_STEPS; i++) {
            this.world.update();
        }
        /* assuming player was created correctly, because previous tests said so */
        final Pair<Double, Double> playerCurrentPosition = player.get().getPosition();
        assertEquals(PLAYER_MOVE, playerTopPosition.getLeft(),  playerCurrentPosition.getLeft(), PRECISION);
        assertEquals(PLAYER_MOVE, playerTopPosition.getRight(), playerCurrentPosition.getRight(), PRECISION);
    }

    /**
     * Test for the block of the transmission of a movement to the left to the player while on a ladder.
     */
    @Test
    public void worldMovementLeftOnLadderTransmission() {
=====================================================================
Found a 9 line (79 tokens) duplication in the following files: 
Starting at line 196 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 217 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

    public void worldMovementJumpTransmission() {
        final Pair<Double, Double> playerInitialPosition = this.playerProperties.getPosition();
        this.world.initLevel(Arrays.asList(this.platformProperties, this.playerProperties));
        final Optional<UnmodifiableEntity> player = this.getPlayer();
        /* updates to let the player fall and touch the platform, if it was created slightly above the platform*/
        for (int i = 0; i < SHORT_UPDATE_STEPS; i++) {
            this.world.update();
        }
        this.world.movePlayer(MovementType.JUMP);
=====================================================================
Found a 9 line (77 tokens) duplication in the following files: 
Starting at line 281 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 304 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 330 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

    public void worldMovementClimbDownOnLadderTransmission() {
        this.world.initLevel(Arrays.asList(this.platformProperties, this.playerProperties, this.ladderProperties));
        final Optional<UnmodifiableEntity> player = this.getPlayer();
        /* updates to let the player fall and touch the platform, if it was created slightly above the platform*/
        for (int i = 0; i < SHORT_UPDATE_STEPS; i++) {
            this.world.update();
        }
        this.world.movePlayer(MovementType.CLIMB_UP);
        for (int i = 0; i < LONG_UPDATE_STEPS; i++) {
=====================================================================
Found a 8 line (74 tokens) duplication in the following files: 
Starting at line 241 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 282 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 305 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 331 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

        this.world.initLevel(Arrays.asList(this.platformProperties, this.playerProperties, this.ladderProperties));
        final Optional<UnmodifiableEntity> player = this.getPlayer();
        /* updates to let the player fall and touch the platform, if it was created slightly above the platform*/
        for (int i = 0; i < SHORT_UPDATE_STEPS; i++) {
            this.world.update();
        }
        this.world.movePlayer(MovementType.CLIMB_UP);
        for (int i = 0; i < SHORT_UPDATE_STEPS; i++) {
=====================================================================
Found a 14 line (71 tokens) duplication in the following files: 
Starting at line 226 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 269 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

        for (int i = 0; i < SHORT_UPDATE_STEPS; i++) {
            this.world.update();
        }
        /* assuming player was created correctly, because previous tests said so */
        final Pair<Double, Double> playerCurrentPosition = player.get().getPosition();
        assertEquals(PLAYER_MOVE, playerInitialPosition.getLeft(), playerCurrentPosition.getLeft(), PRECISION);
        assertEquals(PLAYER_MOVE, playerInitialPosition.getRight(), playerCurrentPosition.getRight(), PRECISION);
    }

    /**
     * Test for the transmission of a climb up command to the player inside the world, when it is in front of a ladder.
     */
    @Test
    public void worldMovementClimbUpWithLadderTransmission() {
=====================================================================
Found a 10 line (66 tokens) duplication in the following files: 
Starting at line 219 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 241 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

        this.world.initLevel(Arrays.asList(this.platformProperties, this.playerProperties));
        final Optional<UnmodifiableEntity> player = this.getPlayer();
        /* updates to let the player fall and touch the platform, if it was created slightly above the platform*/
        for (int i = 0; i < SHORT_UPDATE_STEPS; i++) {
            this.world.update();
        }
        this.world.movePlayer(MovementType.CLIMB_UP);
        for (int i = 0; i < SHORT_UPDATE_STEPS; i++) {
            this.world.update();
        }
=====================================================================
Found a 30 line (66 tokens) duplication in the following files: 
Starting at line 40 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/view/game/PlayerImage.java
Starting at line 28 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/view/game/RollingEnemyImage.java
Starting at line 28 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/view/game/WalkingEnemyImage.java

    PlayerImage(final EntityState state, final String spriteSheetUrl, final int frames) {
        this.state = state;
        this.spriteSheetUrl = spriteSheetUrl;
        this.framesNumber = frames;
    }

    /**
     * Returns the {@link EntityState} the sprite sheet is associated to.
     * @return the {@link EntityState} the sprite sheet returned by {@link #getImageUrl()} is associated to.
     */
    public EntityState getAssociatedEntityState() {
        return this.state;
    }

    /**
     * Returns the URL of the sprite sheet associated to the {@link it.unibo.jmpcoon.model.entities.EntityState}.
     * @return the URL of the sprite sheet associated to the {@link it.unibo.jmpcoon.model.entities.EntityState}
     */
    public String getImageUrl() {
        return SPRITES_DIR + this.spriteSheetUrl;
    }

    /**
     * Returns the number of frames contained in the sprite sheet associated.
     * @return the number of frames contained in the sprite sheet associated 
     */
    public int getFramesNumber() {
        return this.framesNumber;
    }
}
=====================================================================
Found a 7 line (65 tokens) duplication in the following files: 
Starting at line 126 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldCollisionsTest.java
Starting at line 172 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldCollisionsTest.java

        this.world.initLevel(Arrays.asList(this.platformProperties, playerJumpingProperties, rollingEnemyProperties));
        while (this.world.getAliveEntities().size() == 3) {
            this.world.update();
        }
        assertEquals(TWO_ALIVE_ENTITIES, 2, this.world.getAliveEntities().size());
        assertEquals(ONE_DEAD_ENTITY, 1, this.world.getDeadEntities().size());
        assertTrue(ROLLING_DEAD, this.world.getDeadEntities().stream().allMatch(e -> e.getType() == EntityType.ROLLING_ENEMY));
=====================================================================
Found a 7 line (61 tokens) duplication in the following files: 
Starting at line 29 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/DynamicPhysicalBodyCreationTest.java
Starting at line 18 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/PhysicalBodyCreationTest.java
Starting at line 32 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/StaticPhysicalBodyCreationTest.java

public class DynamicPhysicalBodyCreationTest {
    private static final double WORLD_WIDTH = 8;
    private static final double WORLD_HEIGHT = 4.5;
    private static final double STD_WIDTH = WORLD_WIDTH / 15;
    private static final double STD_HEIGHT = WORLD_HEIGHT / 15;
    private static final ImmutablePair<Double, Double> STD_POSITION = new ImmutablePair<>(WORLD_WIDTH / 2, WORLD_HEIGHT / 2);
    private static final double STD_DIMENSION = WORLD_WIDTH / 8;
=====================================================================
Found a 12 line (61 tokens) duplication in the following files: 
Starting at line 134 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 227 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 270 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

            fail(NO_PLAYER);
        }
        final Pair<Double, Double> playerCurrentPosition = player.get().getPosition();
        assertEquals(PLAYER_MOVE, playerInitialPosition.getLeft(), playerCurrentPosition.getLeft(), PRECISION);
        assertEquals(PLAYER_MOVE, playerInitialPosition.getRight(), playerCurrentPosition.getRight(), PRECISION);
    }

    /**
     * Test for the creation of the world with the right characteristics.
     */
    @Test
    public void worldStatusAtCreationTest() {
=====================================================================
Found a 7 line (60 tokens) duplication in the following files: 
Starting at line 29 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/DynamicPhysicalBodyCreationTest.java
Starting at line 18 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/PhysicalBodyCreationTest.java
Starting at line 25 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/PhysicalWorldTest.java
Starting at line 32 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/StaticPhysicalBodyCreationTest.java

public class DynamicPhysicalBodyCreationTest {
    private static final double WORLD_WIDTH = 8;
    private static final double WORLD_HEIGHT = 4.5;
    private static final double STD_WIDTH = WORLD_WIDTH / 15;
    private static final double STD_HEIGHT = WORLD_HEIGHT / 15;
    private static final ImmutablePair<Double, Double> STD_POSITION = new ImmutablePair<>(WORLD_WIDTH / 2, WORLD_HEIGHT / 2);
    private static final double STD_DIMENSION = WORLD_WIDTH / 8;
=====================================================================
Found a 7 line (59 tokens) duplication in the following files: 
Starting at line 260 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 281 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 304 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 330 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

    public void worldMovementClimbDownNotOnLadderTransmission() {
        this.world.initLevel(Arrays.asList(this.platformProperties, this.playerProperties, this.ladderProperties));
        final Optional<UnmodifiableEntity> player = this.getPlayer();
        /* updates to let the player fall and touch the platform, if it was created slightly above the platform*/
        for (int i = 0; i < SHORT_UPDATE_STEPS; i++) {
            this.world.update();
        }
=====================================================================
Found a 5 line (58 tokens) duplication in the following files: 
Starting at line 164 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 180 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

    public void worldMovementRightTransmission() {
        final Pair<Double, Double> playerInitialPosition = this.playerProperties.getPosition();
        this.world.initLevel(Arrays.asList(this.platformProperties, this.playerProperties));
        final Optional<UnmodifiableEntity> player = this.getPlayer();
        this.world.movePlayer(MovementType.MOVE_RIGHT);
=====================================================================
Found a 6 line (56 tokens) duplication in the following files: 
Starting at line 241 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 261 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

        this.world.initLevel(Arrays.asList(this.platformProperties, this.playerProperties, this.ladderProperties));
        final Optional<UnmodifiableEntity> player = this.getPlayer();
        /* updates to let the player fall and touch the platform, if it was created slightly above the platform*/
        for (int i = 0; i < SHORT_UPDATE_STEPS; i++) {
            this.world.update();
        }
=====================================================================
Found a 9 line (53 tokens) duplication in the following files: 
Starting at line 182 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/EntityCreationTest.java
Starting at line 206 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/EntityCreationTest.java
Starting at line 229 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/EntityCreationTest.java

    public void powerUpExtraLifeCreationTest() {
        final AbstractEntityBuilder<PowerUp> powerUpBuilder
            = EntityBuilderUtils.getPowerUpBuilder()
                                .setPosition(STD_POSITION)
                                .setDimensions(STD_RECTANGULAR_DIMENSIONS)
                                .setFactory(this.factory)
                                .setAngle(STD_ANGLE)
                                .setShape(BodyShape.RECTANGLE)
                                .setPowerUpType(Optional.of(PowerUpType.EXTRA_LIFE));
=====================================================================
Found a 8 line (53 tokens) duplication in the following files: 
Starting at line 219 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 282 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 305 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 331 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

        this.world.initLevel(Arrays.asList(this.platformProperties, this.playerProperties));
        final Optional<UnmodifiableEntity> player = this.getPlayer();
        /* updates to let the player fall and touch the platform, if it was created slightly above the platform*/
        for (int i = 0; i < SHORT_UPDATE_STEPS; i++) {
            this.world.update();
        }
        this.world.movePlayer(MovementType.CLIMB_UP);
        for (int i = 0; i < SHORT_UPDATE_STEPS; i++) {
=====================================================================
Found a 4 line (52 tokens) duplication in the following files: 
Starting at line 48 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/DynamicPhysicalBodyCreationTest.java
Starting at line 51 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/StaticPhysicalBodyCreationTest.java

        this.factory = new PhysicalFactoryImpl();
        this.factory.createPhysicalWorld(World.class.cast(new WorldFactoryImpl().create()), WORLD_WIDTH, WORLD_HEIGHT);
        this.allowedCombinations = new HashMap<>();
        this.allowedCombinations.put(EntityType.ROLLING_ENEMY, BodyShape.CIRCLE);
=====================================================================
Found a 4 line (51 tokens) duplication in the following files: 
Starting at line 92 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/PhysicalWorldTest.java
Starting at line 103 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/PhysicalWorldTest.java

    public void alreadyCreatedCollisionRulesFail() {
        final PhysicalWorld physicalWorld = PhysicalWorld.class.cast(this.factory.createPhysicalWorld(this.world, WORLD_WIDTH, WORLD_HEIGHT));
        final PhysicsRulesFactory rulesFactory = new PhysicsRulesFactoryImpl();
        IntStream.range(0, 2).forEach(i -> rulesFactory.createCollisionRules(physicalWorld, this.world));
=====================================================================
Found a 6 line (50 tokens) duplication in the following files: 
Starting at line 206 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/model/entities/AbstractEntityBuilder.java
Starting at line 222 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/model/entities/AbstractEntityBuilder.java

        return this.factory.get().createStaticPhysicalBody(this.center.get(), 
                                                           this.angle.get(), 
                                                           this.shape.get(), 
                                                           this.dimensions.get().getLeft(), 
                                                           this.dimensions.get().getRight(), 
                                                           type,
=====================================================================
Found a 4 line (50 tokens) duplication in the following files: 
Starting at line 164 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 180 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 196 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java
Starting at line 217 of /home/danysk/git/oop18-castellucci-matteo-rondinini-giorgia-rughi-elena-sun/src/it/unibo/jmpcoon/test/WorldTest.java

    public void worldMovementRightTransmission() {
        final Pair<Double, Double> playerInitialPosition = this.playerProperties.getPosition();
        this.world.initLevel(Arrays.asList(this.platformProperties, this.playerProperties));
        final Optional<UnmodifiableEntity> player = this.getPlayer();
